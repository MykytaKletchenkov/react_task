import React, {PropTypes} from "react";
import _ from "lodash";
import {Input} from "../form-elements";
import Avatar from "../user/Avatar";
import "./new-comment.scss";


class NewCommentForm extends React.PureComponent {
    static propTypes = {
        userInfo: PropTypes.any.isRequired,

        createComment: PropTypes.func.isRequired,
        validateNewComment: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = this.getInitState();
        this.validateCommentDebounced = _.debounce(this.validateComment, 500);
    }

    getInitState() {
        return {
            comment: "",

            isValidatingComment: false,
            error: ""
        }
    }

    onNameChanged(event) {
        const comment = event.target.value;

        this.validateCommentDebounced(comment);
        this.setState({comment, isValidatingComment: true});
    }

    onNameKeyPress(event) {
        if (event.key === "Enter") {
            this.createNewComment();
        }
    }

    validateComment(comment) {
        if (!this.state.isValidatingComment) {
            this.setState({isValidatingComment: true});
        }

        this.props.validateNewComment(comment).then(({payload:httpResponse}) => {
            this.setState({isValidatingComment: false, error: httpResponse.error})
        });
    }

    createNewComment() {
        if (this.state.isValidatingComment) {
            this.setState({error: "Please wait till validation will be completed."});
        } else if (!this.state.error) {
            this.props.createComment(this.props.userInfo, this.state.comment).then(({payload:httpResponse}) => {
                if (httpResponse.error) {
                    this.setState({error: httpResponse.error});
                    return;
                }

                this.setState({...this.getInitState()});
            });
        }
    }

    render() {
        const {comment, isValidatingComment, error} = this.state;

        return <div className="new-comment">
            <div className="new-comment__title">Comments</div>

            <div className="new-comment__content">
                <Avatar url={this.props.userInfo.avatarUrl}
                        className="new-comment__avatar"
                />
                <div className="new-comment__input-section">
                    <Input type='text'
                           className="new-comment__input-section__input"
                           placeholder='Add a comment...'
                           value={comment}
                           error={error}
                           onChange={::this.onNameChanged}
                           onKeyPress={::this.onNameKeyPress}
                    />
                    <div className="new-comment__input-section__error">
                        {error}
                    </div>
                </div>
            </div>
        </div>
    }
}

export default NewCommentForm;
