import React, {PropTypes} from "react";
import Avatar from "../user/Avatar";
import {createCommentShape} from "../../_shapes";

import "./comment-list-item.scss";

class CommentListItem extends React.PureComponent {

    static propTypes = {
        comment: createCommentShape().isRequired
    };

    render() {
        const {comment: {id, message, author, createdDate, likesCount, repliesCount, replyList}} = this.props;

        const likeLabel = likesCount > 0 ? `Like (${likesCount})` : "Like";
        const viewRepliesLabel = repliesCount > 0 ? `View Replies (${repliesCount})` : "No Replies";
        return <li className="comment-list-item">
            <div className="comment-list-item__content-wrapper">
                <Avatar className="comment-list-item__content-avatar" url={author.avatarUrl}/>
                <div className="comment-list-item__content">
                    <div className="comment-list-item__content__user-full-name">{`${author.firstName} ${author.lastName}`}</div>
                    <div className="comment-list-item__content__message">{message}</div>
                </div>
            </div>
            <div className="comment-list-item__footer">
                <div className="comment-list-item__footer-item">{likeLabel}</div>
                <div className="comment-list-item__footer-item">Reply</div>
                <div className="comment-list-item__footer-item">{viewRepliesLabel}</div>
            </div>
        </li>
    }
}

export default CommentListItem;
