import React, {PropTypes} from "react";
import "./input.scss";

class Input extends React.PureComponent {
    static propTypes = {
        error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
    }

    render() {
        const {className, error, ...restProps} = this.props;
        const mergedClassName = `input ${error ? "error" : ""} ${restProps.disabled ? "disabled" : ""} ${className || ""}`;
        return <input {...restProps} className={mergedClassName}/>
    }
}

export default Input;
