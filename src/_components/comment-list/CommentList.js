import React, {PropTypes} from "react";
import _ from "lodash";
import CommentListItem from "./CommentListItem";
import Loader from "../loaders/Loader";

import "./comment-list.scss";

class CommentList extends React.PureComponent {
    static propTypes = {
        commentInfo: PropTypes.instanceOf(Map),

        loadComments: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {isLoading: props.idToComment === null};
    }

    componentDidMount() {
        if (this.props.idToComment === null) {
            this.fireCommentsLoading();
        }
    }

    fireCommentsLoading() {
        if (!this.state.isLoading) {
            this.setState({isLoading: true});
        }

        this.props.loadComments().then(({payload:httpResponse}) => {
            this.setState({isLoading: false});
        });
    }

    render() {
        if (this.state.isLoading) {
            return <Loader className="comment-spinner"/>;
        }

        const commentArray = Array.from(this.props.idToComment.values());

        const commentList = _.chain(commentArray)
            .orderBy(['createdDate'], ['desc'])
            .map(comment => <CommentListItem key={comment.id} comment={comment}/>)
            .value();

        return <ul className="comment-list">
            {commentList}
        </ul>
    }
}

export default CommentList;
