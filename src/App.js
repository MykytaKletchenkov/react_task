import React, {Component} from "react";
import {connect} from "react-redux";
import {loadComments, createComment, validateNewComment} from "./_reducers-and-actions/commentInfoActions";
import NewCommentForm from "./_components/new-comment/NewCommentForm";
import CommentList from "./_components/comment-list/CommentList";

/**
 * Main Wrapper
 */
class App extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const {commentInfo, userInfo, loadComments, createComment, validateNewComment} = this.props;
        return <div>
            <NewCommentForm userInfo={userInfo}
                            createComment={createComment}
                            validateNewComment={validateNewComment}
            />

            <CommentList idToComment={commentInfo.idToComment} loadComments={loadComments}/>
        </div>
    }
}

function mapStateToProps(state) {
    return {
        commentInfo: state.commentInfo,
        userInfo: state.userInfo
    }
}

export default connect(mapStateToProps, {loadComments, createComment, validateNewComment})(App);
