import React, {PropTypes} from "react";
import "./avatar.scss";

class Avatar extends React.PureComponent {

    static propTypes = {
        url: PropTypes.string.isRequired
    };

    render() {
        const {className, url, ...restProps} = this.props;
        return <img {...restProps} className={`avatar ${className || ""}`} src={url}/>
    }
}

export default Avatar;
