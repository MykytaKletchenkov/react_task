import {currentUser} from "../_mock-data/userInfo";
import {createReducer} from "./reducerFactory";

// Assume that this info was fetched during the login stage
const INITIAL_STATE = {
    id: currentUser.id,
    firstName: currentUser.firstName,
    lastName: currentUser.lastName,
    avatarUrl: currentUser.avatarUrl
};

const reducerMap = {};

export default createReducer(reducerMap, INITIAL_STATE);