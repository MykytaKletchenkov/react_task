class User {
    constructor(id, firstName, secondName, avatarUrl) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = secondName;
        this.avatarUrl = avatarUrl;
    }
}

export const users = [
    new User(0, "John", "Smith", "https://randomuser.me/api/portraits/men/1.jpg"),
    new User(1, "Alice", "Kelly", "https://randomuser.me/api/portraits/women/1.jpg"),
    new User(2, "Janet", "Ryan", "https://randomuser.me/api/portraits/women/8.jpg")
];

export const currentUser = users[0];