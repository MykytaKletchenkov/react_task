import * as actions from "../_constants/actions";
import {Comment, comments} from "../_mock-data/commentInfo";

export function loadComments() {
    const request = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({data: comments});
        }, 1500);
    });

    return {
        type: actions.LOAD_COMMENTS,
        payload: request
    }
}

export function createComment(currentUser, comment) {
    const request = new Promise((resolve, reject) => {
        setTimeout(() => {
            if (!comment) {
                resolve({error: "Comment cannot be empty."});
            }

            const userComment = new Comment(
                comment,
                currentUser,
                new Date().getTime(),
                0,
                0,
                null
            );
            resolve({data: userComment});
        }, 300);
    });

    return {
        type: actions.CREATE_COMMENT,
        payload: request
    }
}

export function validateNewComment(message) {
    const request = new Promise((resolve, reject) => {
        setTimeout(() => {
            const errorMsg = message && message.length > 25 ? "Comment must be less 25 characters." : null;
            resolve({error: errorMsg});
        }, 180);
    });

    return {
        type: actions.NO_REDUCER,
        payload: request
    }
}