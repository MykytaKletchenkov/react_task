// Actions for AJAX calls with smart components without triggering reducers.
// It must not be listened by any reducer
export const NO_REDUCER = "NO_REDUCER";

export const LOAD_COMMENTS = "LOAD_COMMENTS";
export const CREATE_COMMENT = 'CREATE_COMMENT';
