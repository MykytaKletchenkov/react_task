import React, {PropTypes} from "react";
import "./loader.scss";

class Loader extends React.PureComponent {
    render() {
        const {className, ...restProps} = this.props;
        return <div {...restProps} className={`loader cssload-container ${className || ""}`}>
            <div className="cssload-speeding-wheel"></div>
        </div>
    }
}

export default Loader;
