import {PropTypes} from "react";

export function createUserShape() {
    return PropTypes.shape({
        id: PropTypes.number,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        avatarUrl: PropTypes.string,
    })
}

export function createCommentShape() {
    return PropTypes.shape({
        id: PropTypes.string,
        message: PropTypes.string,
        author: createUserShape(),
        createdDate: PropTypes.number,
        likesCount: PropTypes.number,
        repliesCount: PropTypes.number,
        replyList: PropTypes.arrayOf(PropTypes.any) // not defined as out of this task's scope
    });
}