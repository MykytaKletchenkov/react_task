import _ from "lodash";
import * as actions from "../_constants/actions";
import {createReducer} from "./reducerFactory";

const INITIAL_STATE = {
    idToComment: null
};

const reducerMap = {
    [actions.LOAD_COMMENTS]: (state, httpResponse) => {
        if (httpResponse.error) {
            return state;
        }

        const idToComment = _.reduce(httpResponse.data, (idToComment, comment) => {
            return idToComment.set(comment.id, comment);
        }, new Map());

        return {idToComment};
    },
    [actions.CREATE_COMMENT]: (state, httpResponse) => {
        if (httpResponse.error) {
            return state;
        }

        const newComment = httpResponse.data;

        const updatedIdToComment = new Map(state.idToComment);
        updatedIdToComment.set(newComment.id, newComment);

        return {idToComment: updatedIdToComment}
    }
};

export default createReducer(reducerMap, INITIAL_STATE);