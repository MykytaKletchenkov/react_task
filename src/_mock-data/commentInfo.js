import {users} from "./userInfo";
import generateUuid from "uuid/v1";

export class Comment {
    constructor(message, author, createdDate, likesCount, repliesCount, replyList) {
        this.id = generateUuid();
        this.message = message;
        this.author = author;
        this.createdDate = createdDate;
        this.likesCount = likesCount;
        this.repliesCount = repliesCount;
        this.replyList = replyList;
    }
}

export const comments = [
    new Comment(
        "I really like the way you have broken down the material. How does one model the transition state?",
        users[2],
        1491920394000,
        15,
        2,
        null
    ),
    new Comment(
        "Have you ever tried to carry out an experiment to determine if the flowis turbulent or not?",
        users[1],
        1491914421000,
        37,
        11,
        null
    ),
];